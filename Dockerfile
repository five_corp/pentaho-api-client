FROM node:4.4

RUN DEBIAN_FRONTEND=noninteractive && \
    apt-get -q update && \
    apt-get install -y vim git

VOLUME  /root/src
WORKDIR /root/src
