/**
 References:

  - unirest: https://www.npmjs.com/package/unirest
  - nconf: https://www.npmjs.com/package/nconf
  - ADM-ZIP: https://www.npmjs.com/package/adm-zip
  - xmldoc: https://www.npmjs.com/package/xmldoc
  - Pentaho 5.X API Reference:
    http://javadoc.pentaho.com/bi-platform530/webservice530/
*/

'use strict'

var nconf = require('nconf')
var clients = require('./client.js')

// Load config parameters from "config.json" or command line:
nconf.argv().file({ file: 'config.json' })
nconf.defaults({
  'pentaho': {
    'user': 'admin',
    'pass': 'Changeme',
    'URL': 'http://pentaho.server.com:8080/pentaho/api',
    'prefix': ':home'
  },
  'tenant': {
    'name': 'test',
    'pass': 'randomPass',
    'role': 'tenant',
    'master': 'demo',
    'database': {
      'driver': 'com.mysql.jdbc.Driver',
       // TENANT gets replaced by the tenant name
      'URI': 'jdbc:mysql://TENANT.domain.com:3306/data',
      'user': 'user',
      'pass': 'Changeme'
    }
  }
})

// Dump the config, just for informative purposes
var pentahoConfig = nconf.get('pentaho')
var tenantConfig = nconf.get('tenant')
console.log(`
  pentaho: ${JSON.stringify(pentahoConfig)}
  tenant: ${JSON.stringify(tenantConfig)}
`)

// Build the Pentaho Client and Tenant Builder
var client = new clients.PentahoClient(pentahoConfig)
var tenant = new clients.TenantBuilder(client, tenantConfig)

// Create the tenant with the given password
tenant.create(tenantConfig.pass, 3, 5000)
.then((result) => {
  console.log('Tenant created')
})
.then(() => {
  return tenant.cloneFiles(3, 5000)
})
.then((result) => {
  console.log('Tasks completed')
})
.catch((err) => {
  console.log('Tenant cloning error: ' + err)
})
