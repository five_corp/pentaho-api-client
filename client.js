/**
 References:

  - unirest: https://www.npmjs.com/package/unirest
  - nconf: https://www.npmjs.com/package/nconf
  - ADM-ZIP: https://www.npmjs.com/package/adm-zip
  - xmldoc: https://www.npmjs.com/package/xmldoc
  - Pentaho 5.X API Reference:
    http://javadoc.pentaho.com/bi-platform530/webservice530/
*/

'use strict'

var ospath = require('path')
var fs = require('fs')
var unirest = require('unirest')
var AdmZip = require('adm-zip')
var xmldoc = require('xmldoc')

/**
 * Controls retry of promises
 */
function Retry(times, pause, closure) {
  return closure().catch((err) => {
    // If error && should retry, do retry
    if(times && pause && times > 0) {
      // Pause for the specified time
      return new Promise((resolve, reject) => {
        setTimeout(() => { resolve() }, pause)
      })
      .then(() => {
        return Retry(times-1, pause, closure)
      })
    }
    throw err
  })
}

/**
 * Pentaho API client Class
 */
class PentahoClient {

  /**
   * @param [Object] pentahoConfig {
   *  username: API username
   *  password: API password
   *  URL: API URL (e.g. http://pentaho.domain.com:8080/pentaho/api)
   *  prefix: Top-level directory, in Pentaho notation (e.g. ':public')
   *  tmp: Folder to create temporary files
   * }
   */
  constructor (pentahoConfig) {
    this.apiCredentials = {
      user: pentahoConfig.user,
      pass: pentahoConfig.pass,
      sendImmediately: true
    }
    this.apiURL = pentahoConfig.URL
    this.prefix = pentahoConfig.prefix
    this.temp = pentahoConfig.tmp || '/tmp'
    // Pentaho is quite picky about these headers
    this.headers = {
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'Accept-Encoding': 'gzip, deflate',
      'User-Agent': 'Node LTS with unirest'
    }
    // Prefer JSON over XML for REST API responses
    this.accept = {
      Accept: 'application/json'
    }
  }

  /**
   * Build a pentaho path by joining all the non-empty items
   * in an array with the ':' separator
   */
  buildPath () {
    let args = Array.prototype.slice.call(arguments)
    let path = args.filter((item) => {
      if (item) return true
    }).join(':')
    if (!path.startsWith(':')) path = ':' + path
    return path
  }

  /**
  * Perform a GET request against Pentaho.
  */
  get (method, raw, parameters) {
    let self = this
    return new Promise((resolve, reject) => {
      try {
        let request = unirest.get(self.apiURL + method)
          .strictSSL(false)
          .auth(self.apiCredentials)
          .headers(self.headers)
        if (raw) {
          // in case we are downloading a zip file, we need to set
          // the encoding to "null" to get a raw byte buffer we can
          // unzip successfully
          request.encoding(null)
        } else {
          // If we do not require raw data, try to get json
          request = request.headers(self.accept)
        }
        if (parameters) {
          request = request.send(parameters)
        }
        request.end((response) => {
          resolve(response)
        })
      } catch (err) {
        reject(err)
      }
    })
  }

  /**
   * Uploads some content to Pentaho.
   *
   * - MIME type is text/plain by default.
   * - 'parameters' are passed as a query string.
   */
  put (method, content, mime, parameters) {
    let self = this
    mime = mime || 'text/plain; charset=utf-8'
    return new Promise((resolve, reject) => {
      try {
        let request = unirest.put(self.apiURL + method)
          .strictSSL(false)
          .auth(self.apiCredentials)
          .headers(self.headers)
          .headers(self.accept)
          .headers({ 'Content-Type': mime })
        if (parameters) {
          request = request.query(parameters)
        }
        if (content) {
          request = request.send(content)
        }
        request.end((response) => {
          resolve(response)
        })
      } catch (err) {
        reject(err)
      }
    })
  }

  /**
   * Get the list of files under a repo path.
   *
   * All paths are specified the Pentaho style and prefixed with the
   * configured prefix. I,e if prefix is ':home' and folder is
   * 'tenant:cda', then full path is ':home:tenant:cda'
   *
   * If a list of extensions is specified, return only those files
   * with matching extensions
   */
  filesAt (folder, extensions) {
    let self = this
    let path = self.buildPath(self.prefix, folder)
    return self.get('/repo/files/' + path + '/children')
    .then((message) => {
      if (message.error) {
        throw new Error(message.error)
      }
      let children = message.body['repositoryFileDto']
      if (!children) {
        throw new Error('Missing repository file listing')
      }
      if (!extensions) {
        return children
      }
      // If extension list provided, filter extensions
      return children.filter((child) => {
        for (let ext of extensions) {
          if (child['name'].endsWith(ext)) return true
        }
      })
    })
  }

  /**
   * Download a file from the Pentaho repo.
   *
   * All paths are specified the Pentaho style and prefixed with the
   * configured prefix. I,e if prefix is ':home' and folder is
   * 'tenant:cda', then full path is ':home:tenant:cda'
   */
  download (folder, file) {
    let self = this
    let path = self.buildPath(self.prefix, folder, file)
    return self.get('/repo/files/' + path + '/download', true)
    // Download and unzip the file
    .then((message) => {
      if (message.error) {
        throw new Error(message.error)
      }
      let zip = new AdmZip(message.raw_body)
      let zipEntries = zip.getEntries()
      for (let entry of zipEntries) {
        if (entry.entryName === file) {
          zip.extractEntryTo(entry, self.temp, false, true)
          return entry.entryName
        }
      }
      throw new Error('Download target not in zip file')
    })
    .then((name) => {
      let fullPath = ospath.join(self.temp, name)
      return new Promise((resolve, reject) => {
        fs.readFile(fullPath, 'utf-8', (err, data) => {
          // fs.unlink(fullPath)
          if (err) reject(err)
          else resolve(data)
        })
      })
    })
  }

  /**
   * Create a directory in Pentaho.
   *
   * All paths are specified the Pentaho style and prefixed with the
   * configured prefix. I,e if prefix is ':home' and folder is
   * 'tenant:cda', then full path is ':home:tenant:cda'
   */
  createDir (folder) {
    let path = this.buildPath(this.prefix, folder)
    return this.put('/repo/files/' + path + '/createDir')
    .then((response) => {
      if (response.error) {
        // Error 409 is "folder already exists"
        if (!response.code || response.code != 409) {
          throw new Error(response.error)
        }
      }
      return true
    })
  }

  /**
   * Upload a file to the Pentaho repo.
   *
   * All paths are specified the Pentaho style and prefixed with the
   * configured prefix. I,e if prefix is ':home' and folder is
   * 'tenant:cda', then full path is ':home:tenant:cda'
   */
  upload (folder, file, content) {
    let path = this.buildPath(this.prefix, folder, file)
    return this.put('/repo/files/' + path, content)
    .then((response) => {
      if (response.error) {
        throw new Error(response.error)
      }
      return response
    })
  }

  /**
   * Copy a single file in the repository, optionally applying
   * the given transform function.
   *
   * All paths are specified the Pentaho style and prefixed with the
   * configured prefix. I,e if prefix is ':home' and folder is
   * 'tenant:cda', then full path is ':home:tenant:cda'
   */
  copy (pFrom, pTo, name, transform) {
    transform = transform || ((name, result) => { return result })
    let self = this
    return self.download(pFrom, name)
    .then((result) => {
      console.log(`pentahoClient::copy(${name}): downloaded`)
      return self.upload(pTo, name, transform(name, result))
    })
    .then((result) => {
      console.log('File ' + name + ' transferred sucessfully')
    })
  }

  /**
   * Copy all files in a given folder
   *
   * pFrom and pTo are specified the Pentaho style, i.e /public/demo
   * becomes :public:demo
   *
   * All paths are specified the Pentaho style and prefixed with the
   * configured prefix. I,e if prefix is ':home' and folder is
   * 'tenant:cda', then full path is ':home:tenant:cda'
   */
  copyAll (pFrom, pTo, extensions, transform, retries, pause) {
    let self = this
    return self.filesAt(pFrom, extensions)
    .then((children) => {
      return Promise.all(children.map((child) => {
        return Retry(retries, pause, () => {
          return self.copy(pFrom, pTo, child.name, transform)
        })
      }))
    })
  }

  /**
   * Creates an user with the given username and password.
   * Returns a promise.
   */
  createUser (userName, password) {
    let self = this
    let data = JSON.stringify({
      userName: userName,
      password: password
    })
    return this.put('/userroledao/createUser', data, 'application/json')
    .then((response) => {
      if (response.error) {
        // 403 is the error returned for existing user.
        if (!response.code || response.code != 403) {
          throw new Error(response.error)
        }
        // If existing user, just apply the new password
        return self.updatePassword(userName, password)
      }
      return response
    })
  }

  /**
   * Update user password.
   */
  updatePassword(userName, password) {
    let self = this
    let data = JSON.stringify({
      userName: userName,
      password: password
    })
    return this.put('/userroledao/updatePassword', data, 'application/json')
    .then((response) => {
      if (response.error) {
        throw new Error(response.error)
      }
      return response
    })
  }

  /**
   * Assigns a role to an user
   */
  assignRole(userName, roleName) {
    let params = {
      userName: userName,
      roleNames: roleName
    }
    return this.put('/userroledao/assignRoleToUser', '', 'text/plain', params)
    .then((response) => {
      if (response.error) {
        throw new Error(response.error)
      }
      return response
    })
  }
}

class TenantBuilder {

  /**
   * @param [PentahoClient] pentahoClient
   * @param [Object] tenantConfig {
   *  name: The tenant name.
   *  role: The tenant role.
   *  master: The tenant to use as gold image, for cloning
   *  database: {
   *    driver: Database driver name ('com.mysql.jdbc.Driver')
   *    URI: database URI ('jdbc:mysql://TENANT.domain.com:3306/dbname')
   *      'TENANT' gets replaced by the tenant's name-
   *    user: databse username
   *    pass: database password
   *  }
   * }
   */
  constructor (pentahoClient, tenantConfig) {
    this.client = pentahoClient
    this.master = tenantConfig.master
    this.name = tenantConfig.name
    this.role = tenantConfig.role
    this.db = tenantConfig.database
  }

  /**
   * Transforms CDE document, replacing ocurrences of the
   * master path for the cloned path
   */
  transformCDE (name, content) {
    try {
      let self = this
      if (name.endsWith('.cdfde')) {
        content = JSON.parse(content.trim())
        content['filename'] = self.client.buildPath(
          self.client.prefix, self.name, name)
          .replace(':', '/')
        content = JSON.stringify(content)
      }
    } catch (err) {
      console.log('Error parsing ' + name + ': ' + err)
    }
    return content
  }

  /**
   * Transform CDA document, replacing the database host, name,
   * username and password
   */
  transformCDA (name, content) {
    let self = this
    try {
      let document = new xmldoc.XmlDocument(content)
      let dSource = document.childNamed('DataSources')
      let replacement = [
        { key: 'Driver', val: self.db.driver },
        { key: 'Url', val: self.db.URI.replace('TENANT', self.name) },
        { key: 'User', val: self.db.user },
        { key: 'Pass', val: self.db.pass }
      ]
      let connections = []
      // Replace connection parameters
      for (let connection of dSource.childrenNamed('Connection')) {
        connections.push(connection.attr['id'])
        for (let entry of replacement) {
          connection.childNamed(entry.key).val = entry.val
        }
      }
      // Keep assigning rotating connection ids to DataAccess
      let index = 0
      for(let access of document.childrenNamed('DataAccess')) {
        let connection = connections[index++ % connections.length]
        access.attr['connection'] = connection
      }
      // Serialize document
      content = document.toString()
    } catch (err) {
      console.log('Error parsing ' + name + ': ' + err)
    }
    return content
  }

  /**
   * Creates an user and assigns to the specified role
   */
  create(password, retries, pause) {
    let self = this
    return Retry(retries, pause, () => {
      return self.client.createUser(self.name, password)
    })
    .then(() => {
      return Retry(retries, pause, () => {
        return self.client.assignRole(self.name, self.role)
      })
    })
  }

  /**
   * Clones all the tenant files
   */
  cloneFiles (retries, pause) {
    let self = this
    let promise = Retry(1, pause, () => {
      return self.client.createDir(self.name + ':cda')
    })
    .catch((err) => {
      console.log('Could not create :cda folder: ' + err)
    })
    let folders = [
      { path: '', ext: ['.cdfde', '.wcdf'], xform: 'transformCDE' },
      { path: ':cda', ext: ['.cda'], xform: 'transformCDA' }
    ]
    folders.forEach((folder) => {
      promise = promise.then((result) => {
        return self.client.copyAll(
          self.master + folder.path, self.name + folder.path,
          folder.ext,
          self[folder.xform].bind(self),
          retries, pause
        )
      })
    })
    return promise
  }
}

module.exports.PentahoClient = PentahoClient
module.exports.TenantBuilder = TenantBuilder
