Pentaho API client
==================

Simple helper library to download / upload files to the Pentaho repository, performing some transformations on the fly.

Install using:

```
npm install https://bitbucket.org/five_corp/pentaho-api-client.git
```

Usage
-----

```node
var client = require ('pentaho-api-client')

// Configure and build the Pentaho client
// --------------------------------------
var pentahoConfig = {
  user: 'admin',
  pass: 'Changeme',
  URL: 'http://pentaho.server.address:8080/pentaho/api',
  tmp: '/tmp', // Folder to store temporary files
  /* Repository folder where the tenant's files are located.
   * All operations performed by the client will take place below this
   * directory. Tipically you would use ':home'.
   *
   * Beware that we are using Pentaho syntax, i.e. path separator
   * is the colon (':') character.
   */
  prefix: ':home'
}

var pentahoClient = new client.PentahoClient(pentahoConfig)

// Configure and build the tenant builder
// --------------------------------------
var tenantConfig = {
  name: 'tenant'
  master: 'master', // Name of the tenant to clone
  role: 'tenant',
  database: {
    driver: 'com.mysql.jdbc.Driver',
    // The string 'TENANT' gets replaced by the tenant's name
    URI: 'jdbc:mysql://TENANT.db.server:3306/dbname',
    user: 'db_user',
    pass: 'Changeme'
  }
}

var tenantBuilder = new client.TenantBuilder(pentahoClient, tenantConfig)

// Create the tenant with the given password
tenant.create('randomPass')
.then((result) => {
  console.log('Tenant created')
})
.catch((err) => {
  console.log('Tenant creation error: ' + err)
})

// Clone the master's files to the tenant
// --------------------------------------
tenantBuilder.cloneFiles()
.then((result) => {
  console.log('File cloning complete')
})
.catch((err) => {
  console.log('File cloning error: ' + err)
})
```

As Pentaho API sometime fails without much explanations, I added a '*retries*' and '*pause*' parameters to *PentahoTenant* methods: *create(randomPass, retries, pause)* and *cloneFiles(retries, pause)*. If specified, whenever the function detects an error, retries its action up to *retries* times, waiting *pause* milliseconds between each retry, to throttle requests.
